# README #

This project is for code review and demo purpose only, this is not an actual app.

### What is this repository for? ###

* Code review
* It uses demonstration of recyclerview.
* Default view pager. 
* CardView
* Network calls made using Rerofit2.
* Simple MVP architecture.
* 3 different product flavors.

### How do I get set up? ###

* Clone/Download project
* Android Studio 2.+
* Build Gradle 2.1.2
* appcompat-v7:23.4.0
* recyclerview-v7:23.4.0
* design:23.4.0
* cardview-v7:23.4.0
* retrofit2:retrofit:2.1.0
* gson:gson:2.6.2
* okhttp3:logging-interceptor:3.3.1

### Who do I talk to? ###

* Repo owner or admin "Keyur Tailor"
* contact 
  E-mail - keyur.tailor89@gmail.com
  LinkedIn - https://in.linkedin.com/in/ktailor