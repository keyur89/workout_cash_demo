package com.workout.cash.test.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Keyur on 01-08-2016.
 */

public class GoalResponse implements Serializable{
    @SerializedName("appStatusCode")
    private int appStatusCode;
    @SerializedName("data")
    private List<GoalModel> data;

    public int getAppStatusCode() {
        return appStatusCode;
    }

    public void setAppStatusCode(int appStatusCode) {
        this.appStatusCode = appStatusCode;
    }

    public List<GoalModel> getData() {
        return data;
    }

    public void setData(List<GoalModel> data) {
        this.data = data;
    }
}
