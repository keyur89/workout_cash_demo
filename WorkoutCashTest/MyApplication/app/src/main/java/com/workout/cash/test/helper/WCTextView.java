package com.workout.cash.test.helper;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.widget.TextView;

import com.workout.cash.test.R;

/**
 * Created by Keyur on 01-08-2016.
 */

public class WCTextView extends TextView {

    int fontType = 1;

    public WCTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public WCTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        TypedArray a = context.getTheme().obtainStyledAttributes(attrs, R.styleable.WCTextView, 0, 0);
        try {
            fontType = a.getInt(R.styleable.WCTextView_fontStyle, 0);
        } finally {
            a.recycle();
        }
        init();
    }

    public WCTextView(Context context) {
        super(context);
        init();
    }

    private void init() {
        if (fontType == 0)
            setTypeface(GlobalApp.fontBold);
        else
            setTypeface(GlobalApp.fontNormal);
    }

}

