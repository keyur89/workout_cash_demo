package com.workout.cash.test.activity;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.PagerTabStrip;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.TypedValue;
import android.view.View;
import android.widget.ImageView;

import com.workout.cash.test.R;
import com.workout.cash.test.fragments.AllGoalsFragment;

/**
 * Created by Keyur on 01-08-2016.
 */

public class GoalsActivity extends AppCompatActivity {

    public static String[] TABS = new String[]{"ALL", "TODO"};
    Context mContext;
    ImageView imgBack;
    CoordinatorLayout coordinatorLayout;
    ViewPager viewPager;
    FragmentPagerAdapter adapterViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_goal);

        mContext = this;

        imgBack = (ImageView) findViewById(R.id.imgBack);
        coordinatorLayout = (CoordinatorLayout) findViewById(R.id.coordinatorLayout);
        viewPager = (ViewPager) findViewById(R.id.viewPager);
        PagerTabStrip pager_header = (PagerTabStrip) findViewById(R.id.pager_header);

        adapterViewPager = new GoalsPagerAdapter(getSupportFragmentManager());
        viewPager.setAdapter(adapterViewPager);
        pager_header.setTabIndicatorColorResource(R.color.colorWhite);
        pager_header.setTextColor(getResources().getColor(R.color.colorWhite));
        pager_header.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);

        viewPager.setOffscreenPageLimit(2);
        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    public class GoalsPagerAdapter extends FragmentPagerAdapter {
        private int NUM_ITEMS = TABS.length;

        public GoalsPagerAdapter(FragmentManager fragmentManager) {
            super(fragmentManager);
        }

        // Returns total number of pages
        @Override
        public int getCount() {
            return NUM_ITEMS;
        }

        // Returns the fragment to display for that page
        @Override
        public Fragment getItem(int position) {
            String type = "";
            switch (position) {
                case 0: // Fragment # 0 - This will show All Goals
                    type = "all";
                    break;
                case 1: // Fragment # 0 - This will show Todo Goals
                    type = "todo";
                    break;
            }
            return AllGoalsFragment.newInstance(type);
        }

        // Returns the page title for the top indicator
        @Override
        public CharSequence getPageTitle(int position) {
            return TABS[position];
        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
