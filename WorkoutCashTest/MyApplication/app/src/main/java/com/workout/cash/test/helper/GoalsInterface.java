package com.workout.cash.test.helper;

import com.workout.cash.test.model.AchieveResponse;
import com.workout.cash.test.model.DataResponse;
import com.workout.cash.test.model.GoalResponse;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.PUT;

/**
 * Created by Keyur on 02-08-2016.
 */
public interface GoalsInterface {

    // Goal Listing
    @GET("account/goals")
    @Headers({
            "Content-Type:application/json",
            "access_token:3cb751eb-b0ac-4ecb-90be-484046847663"
    })
    public Call<GoalResponse> getGoalsList();


    // Goal Achieve "I Did It"
    @PUT("account/goals/achieve")
    @Headers({
            "Content-Type:application/json",
            "X-USER-TIMEZONE:GMT",
            "access_token:3cb751eb-b0ac-4ecb-90be-484046847663"
    })
    public Call<AchieveResponse> achieveGoal(@Body Task task);

    // Goal "Subscribe"
    @PUT("account/goals/subscribe")
    @Headers({
            "Content-Type:application/json",
            "access_token:3cb751eb-b0ac-4ecb-90be-484046847663"
    })
    public Call<DataResponse> subscribeGoal(@Body Task task);

    // Goal "UnSubscribe"
    @PUT("account/goals/unSubscribe")
    @Headers({
            "Content-Type:application/json",
            "access_token:3cb751eb-b0ac-4ecb-90be-484046847663"
    })
    public Call<DataResponse> unSubscribeGoal(@Body Task task);

    public class Task {
        private int goalID;
        private String dateTime;

        public Task(int id) {
            this.goalID = id;
        }

        public Task(int id, String dateTime) {
            this.goalID = id;
            this.dateTime = dateTime;
        }
    }
}
