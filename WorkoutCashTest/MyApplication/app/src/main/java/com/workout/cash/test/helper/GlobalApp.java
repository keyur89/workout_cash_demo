package com.workout.cash.test.helper;

import android.app.Application;
import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Typeface;

import com.workout.cash.test.webservice.WebUrl;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Keyur on 01-08-2016.
 */

public class GlobalApp extends Application {
    public static String TAG = "Goal App";
    public static Context mContext;
    public static Typeface fontBold, fontNormal;
    private static GlobalApp instance;
    public static Retrofit retrofit;
    public static Context getAppContext() {
        return GlobalApp.mContext;
    }

    public static GlobalApp getInstance() {
        return instance;
    }


    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }


    @Override
    public void onCreate() {

        super.onCreate();

        instance = this;
        try {
            GlobalApp.mContext = getApplicationContext();
            HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

            OkHttpClient defaultHttpClient = new OkHttpClient.Builder().addInterceptor(interceptor).build();

            retrofit = new Retrofit.Builder().addConverterFactory(GsonConverterFactory.create()).baseUrl(WebUrl.ROOT_URL).client(defaultHttpClient).build();
            try {
                //setting to default system font
                fontBold = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/Proxima Nova Bold.otf");
                fontNormal = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/Proxima Nova Regular.otf");

            } catch (Exception e1) {
                e1.printStackTrace();
                fontBold = Typeface.create(Typeface.SANS_SERIF, Typeface.BOLD);
                fontNormal = Typeface.create(Typeface.SANS_SERIF, Typeface.NORMAL);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
    }
}
