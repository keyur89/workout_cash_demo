package com.workout.cash.test.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Keyur on 01-08-2016.
 */

public class AchieveResponse implements Serializable {
    @SerializedName("appStatusCode")
    private int appStatusCode;
    @SerializedName("data")
    private String data;
    @SerializedName("forDay")
    private String forDay;
    @SerializedName("userId")
    private String userId;
    @SerializedName("calories")
    private String calories;
    @SerializedName("stepCount")
    private String stepCount;
    @SerializedName("distance")
    private String distance;
    @SerializedName("fitnessSweatPoints")
    private String fitnessSweatPoints;

    public String getGoalProgressSweatPoints() {
        return goalProgressSweatPoints;
    }

    public void setGoalProgressSweatPoints(String goalProgressSweatPoints) {
        this.goalProgressSweatPoints = goalProgressSweatPoints;
    }

    public String getBadgeSweatPoints() {
        return badgeSweatPoints;
    }

    public void setBadgeSweatPoints(String badgeSweatPoints) {
        this.badgeSweatPoints = badgeSweatPoints;
    }

    public String getCalories() {
        return calories;
    }

    public void setCalories(String calories) {
        this.calories = calories;
    }

    public String getCurrentSweatPoints() {
        return currentSweatPoints;
    }

    public void setCurrentSweatPoints(String currentSweatPoints) {
        this.currentSweatPoints = currentSweatPoints;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public String getFitnessSweatPoints() {
        return fitnessSweatPoints;
    }

    public void setFitnessSweatPoints(String fitnessSweatPoints) {
        this.fitnessSweatPoints = fitnessSweatPoints;
    }

    public String getForDay() {
        return forDay;
    }

    public void setForDay(String forDay) {
        this.forDay = forDay;
    }

    public String getStepCount() {
        return stepCount;
    }

    public void setStepCount(String stepCount) {
        this.stepCount = stepCount;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    @SerializedName("goalProgressSweatPoints")
    private String goalProgressSweatPoints;
    @SerializedName("badgeSweatPoints")
    private String badgeSweatPoints;
    @SerializedName("currentSweatPoints")
    private String currentSweatPoints;

    public int getAppStatusCode() {
        return appStatusCode;
    }

    public void setAppStatusCode(int appStatusCode) {
        this.appStatusCode = appStatusCode;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }
}
