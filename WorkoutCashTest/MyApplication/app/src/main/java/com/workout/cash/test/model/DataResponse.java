package com.workout.cash.test.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Keyur on 01-08-2016.
 */

public class DataResponse implements Serializable {
    @SerializedName("appStatusCode")
    private int appStatusCode;
    @SerializedName("data")
    private String data;

    public int getAppStatusCode() {
        return appStatusCode;
    }

    public void setAppStatusCode(int appStatusCode) {
        this.appStatusCode = appStatusCode;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }
}
