package com.workout.cash.test.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.workout.cash.test.R;
import com.workout.cash.test.adapter.AllGoalsAdapter;
import com.workout.cash.test.helper.GlobalApp;
import com.workout.cash.test.helper.GoalsInterface;
import com.workout.cash.test.helper.Utility;
import com.workout.cash.test.model.GoalModel;
import com.workout.cash.test.model.GoalResponse;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Keyur on 01-08-2016.
 */

public class AllGoalsFragment extends Fragment {

    Context mContext;
    RecyclerView recyclerView;
    TextView txtNoInternet;
    SwipeRefreshLayout mSwipeLayout;
    LinearLayoutManager layoutManager;
    List<GoalModel> goalList = new ArrayList<>();
    AllGoalsAdapter adapter;
    String type = "";
    boolean isSwipeRefresh = false;

    // newInstance constructor for creating fragment with arguments
    public static AllGoalsFragment newInstance(String type) {
        AllGoalsFragment fragment = new AllGoalsFragment();
        Bundle args = new Bundle();
        args.putString("type", type);
        fragment.setArguments(args);
        return fragment;
    }

    // Store instance variables based on arguments passed
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = getActivity();
        try {
            if (getArguments() != null) {
                type = getArguments().getString("type");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("Type Is " + type);
    }

    // Inflate the view for the fragment based on layout XML
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.frag_all_goals, container, false);
        txtNoInternet = (TextView) view.findViewById(R.id.txtNoInternet);
        recyclerView = (RecyclerView) view.findViewById(R.id.goals_recycler_view);
        mSwipeLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipe_container);

        layoutManager = new LinearLayoutManager(mContext);
        recyclerView.setLayoutManager(layoutManager);
        adapter = new AllGoalsAdapter(AllGoalsFragment.this, goalList, mContext);
        recyclerView.setAdapter(adapter);

        mSwipeLayout.setColorSchemeResources(R.color.blue_dark);
        mSwipeLayout.setDistanceToTriggerSync(200);
        mSwipeLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                isSwipeRefresh = true;
                refreshAll();
            }
        });

        //First Time load data
        refreshAll();

        return view;
    }

    public void refreshAll() {
        if (Utility.isNetworkAvailable(mContext)) {
            getGoalsList();
            recyclerView.setVisibility(View.VISIBLE);
            txtNoInternet.setVisibility(View.GONE);
        } else {
            Toast.makeText(mContext, getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
            recyclerView.setVisibility(View.GONE);
            txtNoInternet.setVisibility(View.VISIBLE);
        }
    }

    private void getGoalsList() {
        //While the app fetched data we are displaying a progress dialog
        final ProgressDialog loading = new ProgressDialog(mContext);
        loading.setMessage(mContext.getResources().getString(R.string.loading));
        if (!isSwipeRefresh)
            loading.show();
        // goalList.clear();
        GoalsInterface service = GlobalApp.retrofit.create(GoalsInterface.class);

        Call<GoalResponse> call = service.getGoalsList();
        call.enqueue(new Callback<GoalResponse>() {
            @Override
            public void onResponse(Call<GoalResponse> call, Response<GoalResponse> response) {
                loading.dismiss();

                try {
                    int statusCode = response.code();
                    int appStatusCode = response.body().getAppStatusCode();
                    goalList = response.body().getData();
                    if (goalList.size() > 0) {
                        recyclerView.setVisibility(View.VISIBLE);
                        txtNoInternet.setVisibility(View.GONE);
                        List<GoalModel> finalGoalList = new ArrayList<>();

                        if (type.equalsIgnoreCase("todo")) {
                            try {
                                //Just show Pending Goals in TODO tab
                                for (int i = 0; i < goalList.size(); i++) {
                                    Calendar calToday = Calendar.getInstance();
                                    Calendar calCompleted = Calendar.getInstance();
                                    calToday.setTime(Utility.dbDateFormat.parse(Utility.dbDateFormat.format(calToday.getTime())));
                                    if (goalList.get(i).getStatus() == 2) {
                                        if (goalList.get(i).getLastCompletedDate() != null) {
                                            calCompleted.setTime(Utility.dbDateFormat.parse(goalList.get(i).getLastCompletedDate()));
                                            if (calCompleted.getTimeInMillis() < calToday.getTimeInMillis()) {
                                                finalGoalList.add(goalList.get(i));
                                            }
                                        } else {
                                            finalGoalList.add(goalList.get(i));
                                        }
                                    }
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else
                            finalGoalList = goalList;

                        adapter = new AllGoalsAdapter(AllGoalsFragment.this, finalGoalList, mContext);
                        recyclerView.setAdapter(adapter);
                        adapter.notifyDataSetChanged();
                    } else {
                        recyclerView.setVisibility(View.GONE);
                        txtNoInternet.setVisibility(View.VISIBLE);
                        txtNoInternet.setText(getResources().getString(R.string.no_data));
                    }

                    mSwipeLayout.setRefreshing(false);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<GoalResponse> call, Throwable t) {
                loading.dismiss();
                mSwipeLayout.setRefreshing(false);
                t.printStackTrace();
                recyclerView.setVisibility(View.GONE);
                txtNoInternet.setVisibility(View.VISIBLE);
                txtNoInternet.setText(getResources().getString(R.string.unexpected_error));
            }
        });
    }

}
