package com.workout.cash.test.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Keyur on 02-08-2016.
 */
public class GoalModel implements Serializable{

    /* {
         "goalID": 7,
             "title": "Dare to Stair",
             "description": "Dare to use the stairs. Earn sweat points in the same day as your confirm you used stairs today when you had the oppurtunity.",
             "frequency": 1,
             "sweatPoints": 4,
             "weekendBooster": 2,
             "status": 2,
             "createdAt": "2016-05-10T06:58:47Z",
             "updatedAt": "2016-05-10T06:58:47Z",
             "lastCompletedDate": "2016-06-27"
     }*/
    @SerializedName("goalID")
    int goalID;
    @SerializedName("title")
    String title;
    @SerializedName("description")
    String description;
    @SerializedName("frequency")
    int frequency;
    @SerializedName("sweatPoints")
    int sweatPoints;
    @SerializedName("weekendBooster")
    int weekendBooster;
    @SerializedName("status")
    int status;
    @SerializedName("lastCompletedDate")
    String lastCompletedDate;

    public String getLastCompletedDate() {
        return lastCompletedDate;
    }

    public void setLastCompletedDate(String lastCompletedDate) {
        this.lastCompletedDate = lastCompletedDate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getFrequency() {
        return frequency;
    }

    public void setFrequency(int frequency) {
        this.frequency = frequency;
    }

    public int getGoalID() {
        return goalID;
    }

    public void setGoalID(int goalID) {
        this.goalID = goalID;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getSweatPoints() {
        return sweatPoints;
    }

    public void setSweatPoints(int sweatPoints) {
        this.sweatPoints = sweatPoints;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getWeekendBooster() {
        return weekendBooster;
    }

    public void setWeekendBooster(int weekendBooster) {
        this.weekendBooster = weekendBooster;
    }

}
