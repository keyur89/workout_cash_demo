package com.workout.cash.test.adapter;

import android.app.ProgressDialog;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.workout.cash.test.R;
import com.workout.cash.test.fragments.AllGoalsFragment;
import com.workout.cash.test.helper.GlobalApp;
import com.workout.cash.test.helper.GoalsInterface;
import com.workout.cash.test.helper.Utility;
import com.workout.cash.test.model.AchieveResponse;
import com.workout.cash.test.model.DataResponse;
import com.workout.cash.test.model.GoalModel;

import java.util.Calendar;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Keyur on 01-08-2016.
 */

public class AllGoalsAdapter extends RecyclerView.Adapter<AllGoalsAdapter.ViewHolder> {

    private List<GoalModel> goalList;
    private Context mContext;
    AllGoalsFragment fragment;

    public AllGoalsAdapter(AllGoalsFragment fragment,List<GoalModel> m_goalList, Context context) {
        this.fragment = fragment;
        this.goalList = m_goalList;
        this.mContext = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_goal, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        try {
            final GoalModel model = goalList.get(position);
            holder.txtGoalTitle.setText(model.getTitle());
            holder.txtGoalDescription.setText(model.getDescription());
            holder.txtSweatPoints.setText("+ " + model.getSweatPoints());

            holder.btnSubscribe.setVisibility(model.getStatus() == 2 ? View.GONE : View.VISIBLE);
            holder.btnUnsubscribe.setVisibility(model.getStatus() == 2 ? View.VISIBLE : View.GONE);
            holder.txtCompleted.setVisibility(model.getStatus() == 2 ? View.VISIBLE : View.INVISIBLE);

            Calendar calToday = Calendar.getInstance();
            Calendar calCompleted = Calendar.getInstance();
            calToday.setTime(Utility.dbDateFormat.parse(Utility.dbDateFormat.format(calToday.getTime())));
            boolean isCompleted = false;
            if (model.getLastCompletedDate() != null) {
                if (model.getStatus() == 2 && !model.getLastCompletedDate().equalsIgnoreCase("")) {
                    calCompleted.setTime(Utility.dbDateFormat.parse(model.getLastCompletedDate()));
                    if (calCompleted.getTimeInMillis() < calToday.getTimeInMillis()) {
                        isCompleted = false;
                        holder.txtCompleted.setText(mContext.getResources().getString(R.string.i_did_it));
                    } else {
                        isCompleted = true;
                        holder.txtCompleted.setText(mContext.getResources().getString(R.string.well_done));
                    }
                }
            } else {
                isCompleted = false;
                holder.txtCompleted.setText(mContext.getResources().getString(R.string.i_did_it));
            }
            holder.btnSubscribe.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (Utility.isNetworkAvailable(mContext))
                        subscribeGoalTask(model.getGoalID(), position);
                    else
                        Toast.makeText(mContext, mContext.getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                }
            });

            holder.btnUnsubscribe.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (Utility.isNetworkAvailable(mContext))
                        unSubscribeGoalTask(model.getGoalID(), position);
                    else
                        Toast.makeText(mContext, mContext.getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                }
            });

            final boolean finalIsCompleted = isCompleted;
            holder.txtCompleted.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!finalIsCompleted) {
                        if (Utility.isNetworkAvailable(mContext))
                            achieveGoalTask(model.getGoalID(), Utility.wsDateFormat.format(Calendar.getInstance().getTime()), position);
                        else
                            Toast.makeText(mContext, mContext.getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return goalList.size();
    }

    public boolean removeItem(int position) {
        if (goalList.size() >= position + 1) {
            goalList.remove(position);
            return true;
        }
        return false;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        TextView txtGoalTitle;
        TextView txtGoalDescription;
        TextView txtSweatPoints;
        ImageView btnUnsubscribe;
        TextView btnSubscribe;
        TextView txtCompleted;

        public ViewHolder(View v) {
            super(v);
            txtGoalTitle = (TextView) v.findViewById(R.id.txtGoalTitle);
            txtGoalDescription = (TextView) v.findViewById(R.id.txtGoalDescription);
            txtSweatPoints = (TextView) v.findViewById(R.id.txtSweatPoints);
            btnSubscribe = (TextView) v.findViewById(R.id.btnSubscribe);
            btnUnsubscribe = (ImageView) v.findViewById(R.id.btnUnsubscribe);
            txtCompleted = (TextView) v.findViewById(R.id.txtCompleted);
        }
    }

    public void subscribeGoalTask(int goalId, final int position) {
        //While the app fetched data we are displaying a progress dialog
        final ProgressDialog loading = new ProgressDialog(mContext);
        loading.setMessage(mContext.getResources().getString(R.string.loading));
        loading.show();
        GoalsInterface service = GlobalApp.retrofit.create(GoalsInterface.class);
        Call<DataResponse> call = service.subscribeGoal(new GoalsInterface.Task(goalId));
        call.enqueue(new Callback<DataResponse>() {
            @Override
            public void onResponse(Call<DataResponse> call, Response<DataResponse> response) {
                try {
                    loading.dismiss();
                    if (response.body().getAppStatusCode() == 0) {
                        goalList.get(position).setStatus(2);
                        notifyItemChanged(position);
                        fragment.refreshAll();
                    } else
                        Toast.makeText(mContext, mContext.getResources().getString(R.string.unexpected_error), Toast.LENGTH_SHORT).show();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<DataResponse> call, Throwable t) {
                loading.dismiss();
                t.printStackTrace();
                Toast.makeText(mContext, mContext.getResources().getString(R.string.unexpected_error), Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void unSubscribeGoalTask(int goalId, final int position) {
        //While the app fetched data we are displaying a progress dialog
        final ProgressDialog loading = new ProgressDialog(mContext);
        loading.setMessage(mContext.getResources().getString(R.string.loading));
        loading.show();
        GoalsInterface service = GlobalApp.retrofit.create(GoalsInterface.class);
        Call<DataResponse> call = service.unSubscribeGoal(new GoalsInterface.Task(goalId));
        call.enqueue(new Callback<DataResponse>() {
            @Override
            public void onResponse(Call<DataResponse> call, Response<DataResponse> response) {
                try {
                    loading.dismiss();
                    if (response.body().getAppStatusCode() == 0) {
                        goalList.get(position).setStatus(3);
                        notifyItemChanged(position);
                        fragment.refreshAll();
                    } else
                        Toast.makeText(mContext, mContext.getResources().getString(R.string.unexpected_error), Toast.LENGTH_SHORT).show();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<DataResponse> call, Throwable t) {
                loading.dismiss();
                t.printStackTrace();
                Toast.makeText(mContext, mContext.getResources().getString(R.string.unexpected_error), Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void achieveGoalTask(int goalId, String dateTime, final int position) {
        //While the app fetched data we are displaying a progress dialog
        final ProgressDialog loading = new ProgressDialog(mContext);
        loading.setMessage(mContext.getResources().getString(R.string.loading));
        loading.show();
        GoalsInterface service = GlobalApp.retrofit.create(GoalsInterface.class);
        Call<AchieveResponse> call = service.achieveGoal(new GoalsInterface.Task(goalId, dateTime));
        call.enqueue(new Callback<AchieveResponse>() {
            @Override
            public void onResponse(Call<AchieveResponse> call, Response<AchieveResponse> response) {
                try {
                    loading.dismiss();
                    if (response.body().getAppStatusCode() == 0) {
                        goalList.get(position).setLastCompletedDate(Utility.dbDateFormat.format(Calendar.getInstance().getTime()));
                        notifyItemChanged(position);
                        fragment.refreshAll();
                    } else
                        Toast.makeText(mContext, mContext.getResources().getString(R.string.unexpected_error), Toast.LENGTH_SHORT).show();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<AchieveResponse> call, Throwable t) {
                loading.dismiss();
                t.printStackTrace();
                Toast.makeText(mContext, mContext.getResources().getString(R.string.unexpected_error), Toast.LENGTH_SHORT).show();
            }
        });
    }
}